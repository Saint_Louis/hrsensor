﻿import QtQuick 2.0

Rectangle {
    //滑动条基础数据
    property double min: 0
    property double max: 1
    property double value: 0.5

    property double capacity: 1
    property double occupy: 0.2

    id: scrollbar
    color: "#eeeeee"
    radius: 4

    function setValue(newValue){
        if(value !== newValue)
            value = Math.min(max, Math.max(min, newValue));
    }

    Rectangle {

        id: handle
        x: ((scrollbar.value - scrollbar.min) / (scrollbar.max - scrollbar.min)) * (scrollbar.width - width)
        width: parent.width * (parent.occupy / parent.capacity)
        color: "#ffffff"
        radius: 4
        anchors.bottomMargin: 2
        anchors.topMargin: 2
        anchors.bottom: parent.bottom
        anchors.top: parent.top

        state: "NORMAL"

        states:[
            State{
                name:"NORMAL"
                PropertyChanges {target: handle;color:"#FFFFFF"}
            },
            State{
                name: "HOVERED"
                PropertyChanges {target: handle;color:"#CCCCCC"}
            },
            State{
                name: "DRAGING"
                PropertyChanges {target: handle;color:"#AAAAAA"}
            }
        ]

        transitions: [
            Transition {
                to: "*"
                ColorAnimation {target: handle; duration: 100}
            }
        ]

        MouseArea {
            property bool isDraging: false
            property bool isHovered: false
            property int lastPressPos : 0

            anchors.fill: parent
            hoverEnabled: true

            onEntered: {
                isHovered = true;
                updateColor();
            }

            onExited: {
                isHovered = false;
                updateColor();
            }

            onPressed: {
                isDraging = true;
                updateColor();

                lastPressPos = mouseX;
            }

            onReleased: {
                isDraging = false;
                updateColor();
            }

            onMouseXChanged: {
                if(isDraging){
                    if(scrollbar.capacity <= scrollbar.occupy){
                        scrollbar.setValue(0);
                    }else{
                        //通过位移计算新的滑杆位置
                        var newx = handle.x + (mouseX - lastPressPos);
                        //滑杆位置计算值
                        var newValue = newx / (scrollbar.width - width);
                        newValue = newValue * (scrollbar.max - scrollbar.min) + scrollbar.min;
                        //设置新值
                        scrollbar.setValue(newValue);
                    }
                }
            }

            function updateColor(){
                if(isDraging){
                    //handle.color = "#AAAAAA"
                    handle.state = "DRAGING"
                }else{
                    if(isHovered){
                        //handle.color = "#CCCCCC"
                        handle.state = "HOVERED"
                    }else {
                        //handle.color = "#FFFFFF"
                        handle.state = "NORMAL"
                    }
                }
            }
        }
    }
}
