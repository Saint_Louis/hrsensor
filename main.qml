﻿import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.3
import QtQuick.Dialogs 1.2
import QtCharts 2.3

import Louis.HRSensor 1.0;

ApplicationWindow {
    id: applicationWindow
    visible: true
    width: 640
    height: 480
    color: "#f5f5f5"
    title: "生理传感器数据可视化程序"

    menuBar: CustomMenuBar{
        CustomMenu{
            title:"文件(&F)"
            Action{
                text:"保存数据"
                onTriggered: {
                    if(dataRecoder.size() > 0){
                        fileDialog.visible = true;
                    }else{
                        console.log("记录数据为空，停止保存文件");
                    }
                }
            }
        }
        CustomMenu{
            title:"编辑(&E)"
            Action{
                text:"清空图表"
                onTriggered: {
                    clear();
                }
            }
        }
        CustomMenu{
            title:"设置"
            Action{text:"首选项"}
        }

        CustomMenu{
            title: "窗口"
            Action{
                text: "工控面板"
                onTriggered: {
                    icPanel.show();
                }
            }
        }
    }

    CustomPlot{
        id: plot
        anchors.fill: parent

        Component.onCompleted: plot.initCustomPlot();
    }

    //ScrollBar{
    //    id: scrollBar
    //    height: 10
    //    anchors.right: parent.right
    //    anchors.left: parent.left
    //    anchors.bottom: parent.bottom
    //
    //    occupy: chartView.timeScale
    //
    //    onValueChanged: {
    //        console.log(scrollBar.value + "/" + scrollBar.max);
    //        chartView.navigation = Math.max(0, value - chartView.timeScale);
    //    }
    //}

    FileDialog{
        id:fileDialog
        title: "选择文件保存位置："
        folder: shortcuts.desktop
        selectExisting: false
        nameFilters: ["逗号分隔值文件(*.csv)"]
        onAccepted: {
            console.log(dataRecoder.saveToFile(fileDialog.fileUrl.toString().substring(8)));
        }
    }

    DataRecoder{
        id: dataRecoder
    }

    DataAnalyst{
        property variant arr: []
        id:dataAnalyst

        onHeartBeat: {
            //记录数据
            dataRecoder.appendHeartBeat(value);
            plot.appendHeartBeat(value);
        }

        onOffset: {
            plot.appendOffset(value);
        }

        onMeanHeartBeat: {
            dataRecoder.appendMean(value);
            plot.appendMean(value);
        }

        onNormalHeartBeat: {
            dataRecoder.apeendNormalHeartBeat(value);
            plot.appendNormal(value);
            arr.push(value);

            //10秒窗体
            if(arr.length >= 10){
                var sum = 0;
                for(var i = 0; i < arr.length; ++i) sum += arr[i];
                var mean = sum / arr.length;

                dataRecoder.appendUserHeartBeat(mean);
                heartBeatIcon.setHeatBeat(mean);
                plot.appendUserHreatBeat(mean);
                arr.length = 0;
            }
        }
    }

    DataReceiver{
        id: dataReceiver

        property date now : new Date()
        onHeartBeatDataReceive: {
            var gap = (time - now.getTime())/1000.0;

            plot.beginPlot(gap);
            dataRecoder.beginLog(gap);
            dataAnalyst.input(gap, value);
            dataRecoder.endLog();
            plot.endPlot();
        }

        //onRawDataReceive: {
        //    var gap = (new Date() - now.getTime())/1000.0;
        //
        //    rawSeries.clear();
        //    var step = 1.0 / arr.length;
        //    for(var i = 0; i < Math.min(arr.length, 200); ++i){
        //        rawSeries.append(gap - (arr.length - i) * step, arr[i]);
        //    }
        //}
    }

    Item{
        id: heartBeatIcon
        width: 60
        height: 60
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 20

        function setHeatBeat(val){
            if(val <= 0){
                if(animPack.running){
                    animPack.pause();
                }
            }else{
                //计算心率的秒周期，单位是毫秒
                var t = 60 / val * 1000;
                anim1.duration = Math.round(t * 2 / 15);
                anim2.duration = Math.round(t * 1 / 15);
                anim3.duration = Math.round(t * 2 / 15);
                anim4.duration = Math.round(t * 10 / 15);
                animPack.restart();
            }
            heartBeatText.text = Math.round(val).toString();
        }

        Image {
            id: image
            width: parent.width
            height: parent.height
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            source: "imgs/heart2.png"
            fillMode: Image.PreserveAspectFit

            SequentialAnimation on width{
                id: animPack
                running: false
                loops: Animation.Infinite
                NumberAnimation{id:anim1; to: heartBeatIcon.width - 10; easing.type: Easing.Linear; duration: 100}
                PauseAnimation {id:anim2; duration: 50}
                NumberAnimation{id:anim3; to: heartBeatIcon.width; easing.type: Easing.Linear; duration: 100}
                PauseAnimation {id:anim4; duration: 500}
            }

            Text {
                id:heartBeatText
                color: "#ffffff"
                text: "0"
                anchors.verticalCenterOffset: -5
                font.pointSize: 16
                font.bold: true
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
            }
        }
    }

    ICPanel{
        id:icPanel

        //Connections{
        //    target: dataReceiver
        //    onIcStatusUpdate:{
        //        icPanel.icModel.processWorkStatusInfo(info);
        //    }
        //}

        Component.onCompleted: icModel.connectToReceiver(dataReceiver)
    }

    function clear(){
        dataReceiver.now = new Date();
        dataAnalyst.clear();
        dataRecoder.clear();
        plot.clear();
        heartBeatIcon.setHeatBeat(0);
    }
}

/*##^##
Designer {
    D{i:12;anchors_height:200}D{i:13;anchors_y:18}
}
##^##*/
