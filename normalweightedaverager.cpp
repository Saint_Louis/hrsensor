﻿#include "normalweightedaverager.h"
#include <QDebug>

const double PI = atan(1.0) * 4;

//插值函数
double lerp(double a, double b, double t){
    return a + t * (b - a);
}

NormalWeightedAverager::NormalWeightedAverager(QObject* parent) : QObject(parent){
    calNormals();
}

NormalWeightedAverager::NormalWeightedAverager(int wndSize, double miu, double sigma, double xScale, QObject* parent) :
    QObject(parent), m_wndSize(wndSize), m_miu(miu), m_sigma(sigma), m_xScale(xScale) {
    calNormals();
}

void NormalWeightedAverager::clear(){
    m_dataWnd.clear();
}

void NormalWeightedAverager::input(double val){
    m_dataWnd.push_back(val);
    //出栈多余的数据
    if(m_dataWnd.size() > m_wndSize){
        m_dataWnd.pop_front();
    }
}

double NormalWeightedAverager::calculate() const{
    //循环计算权重
    auto dataIt = m_dataWnd.rbegin();
    auto weightIt = m_weights.rbegin();
    double weightSum = 0;
    double sum = 0;
    for(;dataIt != m_dataWnd.rend() && weightIt != m_weights.rend(); ++dataIt, ++weightIt){
        //值为当前数据*权重的总和
        sum += *dataIt * *weightIt;
        weightSum += *weightIt;
    }

    double res = sum / weightSum;
    return res;
}

double NormalWeightedAverager::logNormal(double x, double miu, double sigma){
    return 1.0 / (sqrt(2 * PI) * sigma) * exp(-pow(x - miu, 2) / (2*sigma*sigma));
}

void NormalWeightedAverager::calNormals(){
    double x1 = -m_xScale;
    double x2 = 0;

    m_weights.clear();
    //m_weightsROS = 0;
    for(int i = 0; i < m_wndSize; ++i){
        double loc = i / static_cast<double>(m_wndSize - 1);
        double x = lerp(x1, x2, loc);
        double y = logNormal(x, m_miu, m_sigma);

        m_weights.push_back(y);
        //m_weightsROS += y;
    }
}
