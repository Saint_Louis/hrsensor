﻿#ifndef NORMALWEIGHTEDAVERAGER_H
#define NORMALWEIGHTEDAVERAGER_H
#include <QList>
#include <QObject>

/**
 * @brief 正态权重的平均值计算器
 */
class NormalWeightedAverager : public QObject{
    Q_OBJECT
public:
    NormalWeightedAverager(QObject* parent = nullptr);
    NormalWeightedAverager(int wndSize, double miu, double sigma, double xScale, QObject* parent = nullptr);

    void clear();
    void input(double val);
    double calculate() const;

private:
    QList<double> m_dataWnd;
    QList<double> m_weights;
    int m_wndSize = 10;

    //正态分布参数
    double m_miu = 0.0;
    double m_sigma = 1.0;
    double m_xScale = 3.0;

    static double logNormal(double x, double miu, double sigma);
    void calNormals();
};

#endif // NORMALWEIGHTEDAVERAGER_H
