﻿#ifndef BASEPLOTITEM_H
#define BASEPLOTITEM_H

#include <QQuickPaintedItem>
#include "qcustomplot.h"

class BasePlotItem : public QQuickPaintedItem {
    Q_OBJECT
    //Q_PROPERTY(double maxTime READ getMaxTime)
    //Q_PROPERTY(double timeScale READ getTimeScale WRITE setTimeScale)
    //Q_PROPERTY(double navigation READ getNavigation WRITE setNavigation)
public:
    BasePlotItem(QQuickItem* parent = nullptr);
    virtual ~BasePlotItem() override;

    void paint(QPainter *painter) override;

    Q_INVOKABLE void beginPlot(float time);
    Q_INVOKABLE void endPlot();

    Q_INVOKABLE void initCustomPlot();
    Q_INVOKABLE void appendHeartBeat(double value);
    Q_INVOKABLE void appendOffset(double value);
    Q_INVOKABLE void appendMean(double value);
    Q_INVOKABLE void appendNormal(double value);
    Q_INVOKABLE void appendUserHreatBeat(double value);

    Q_INVOKABLE void clear();

    //double getMaxTime() const;
    //
    //double getTimeScale() const;
    //void setTimeScale(double ts);
    //
    //double getNavigation() const;
    //void setNavigation(double nav);

private:
    QCustomPlot* m_customPlot;
    float m_time;
    //数据的最大时间（X轴最大值）
    //double m_maxTime = 10;
    //时间缩放尺度（X轴显示范围）
    //double m_timeScale = 10;
    //时间导航位置（X轴目前位置）
    //double m_navigation = 0;

    void routeMouseEvent(QMouseEvent* event);
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void mouseDoubleClickEvent(QMouseEvent* event) override;
    void wheelEvent(QWheelEvent* event) override;

private slots:
    void updateCustomPlotSize();
    void onCustomReplot();
};

#endif // BASEPLOTITEM_H
