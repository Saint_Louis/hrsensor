﻿import QtQuick 2.0
import QtQuick.Window 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import Louis.HRSensor 1.0

Window {
    property WorkStatusInfoModel icModel : WorkStatusInfoModel{}
    property DataReceiver dataReceiver: null
    id: window
    //modality: Qt.WindowModal
    width: 300
    height: 340
    title: qsTr("核心程序工控面板")

    ListView{
        property var widths: [120, 180]
        property color gridColor: "grey"

        id: listView
        anchors.fill: parent
        topMargin: 5
        spacing: 10

        //header: Rectangle{
        //    id: rectangle
        //    color: "#48c6ef"
        //    anchors.right: parent.right
        //    anchors.left: parent.left
        //    gradient: Gradient {
        //        GradientStop {
        //            position: 0
        //            color: "#48c6ef"
        //        }
        //
        //        GradientStop {
        //            position: 1
        //            color: "#6f86d6"
        //        }
        //    }
        //
        //    height: del_row.implicitHeight + 10
        //    Row{
        //        id: del_row
        //        spacing: 5
        //        anchors.verticalCenter: parent.verticalCenter
        //        Repeater{
        //            model:[qsTr("工控字段"), qsTr("工控数据")]
        //            Label {
        //                width: listView.widths[index]
        //                id: name
        //                text: modelData
        //            }
        //        }
        //    }
        //}

        model: window.icModel

        //model: ListModel{
        //    ListElement{
        //        title: "freq"
        //        value: "false"
        //    }
        //
        //    ListElement{
        //        title: "range"
        //        value: "false"
        //    }
        //
        //    ListElement{
        //        title: "stability"
        //        value: "false"
        //    }
        //
        //    ListElement{
        //        title: "amount"
        //        value: "false"
        //    }
        //}

        delegate: Item {
            id: element
            width:parent.width
            height: del_title.implicitHeight

            Rectangle{
                anchors.fill: parent
                color: same ? "white" : "#b9fdb9"
                anchors.bottomMargin: -5
                anchors.topMargin: -5
            }

            //网格（水平）
            Rectangle{
                height: 1
                color: listView.gridColor
                anchors.bottomMargin: -listView.spacing * 0.5
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.left: parent.left
            }

            ////网格（垂直）
            //Repeater{
            //    model:2
            //
            //    Rectangle{
            //        x: getX(index)
            //        width: 1
            //        color: listView.gridColor
            //        anchors.top: parent.top
            //        anchors.bottom: parent.bottom
            //
            //        function getX(_index){
            //            var _sum = 0;
            //            for(var i = 0; i <= _index; ++i){
            //                _sum += listView.widths[i];
            //            }
            //            return _sum;
            //        }
            //    }
            //}

            Row{
                //anchors.fill: parent
                height: del_title.implicitHeight
                width: listView.widths[0] + listView.widths[1]
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id:del_title
                    width: listView.widths[0]
                    text: "  " + title
                    font.pointSize: 12
                    color: "black"
                }

                Label{
                    width: listView.widths[1]
                    text: value
                    horizontalAlignment: Text.AlignHCenter
                    font.pointSize: 12
                    color: "black"
                }
            }
        }
    }
}




