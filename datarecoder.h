﻿#ifndef DATARECODER_H
#define DATARECODER_H

#include <QObject>
#include <QList>

/**
 * @brief 心率数据点
 */
struct DataPoint{
    float time;
    float rawHb;
    float meanHb;
    float normalHb;
    float userHb;
};

class DataRecoder : public QObject{
    Q_OBJECT
public:
    explicit DataRecoder(QObject *parent = nullptr);

    Q_INVOKABLE void beginLog(float time);
    Q_INVOKABLE void endLog();

    Q_INVOKABLE void appendHeartBeat(float value);
    Q_INVOKABLE void appendMean(float value);
    Q_INVOKABLE void apeendNormalHeartBeat(float value);
    Q_INVOKABLE void appendUserHeartBeat(float value);

    Q_INVOKABLE void clear();
    Q_INVOKABLE int size() const;
    Q_INVOKABLE bool saveToFile(const QString& path) const;

private:
    bool m_loging = false;
    QList<DataPoint> m_dataPoints;
    //QList<std::pair<float, int>> m_hbRecord;
    //QList<std::pair<float, int>> m_offsetRecord;
    //QList<std::pair<float, float>> m_meanHeatBeat;
    //QList<std::pair<float, float>> m_normalHeatBeat;
};

#endif // DATARECODER_H
