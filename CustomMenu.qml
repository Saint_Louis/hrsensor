﻿import QtQuick 2.0
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.0

Menu{
    title: qsTr("工具(&T)")

    background: Item{
        implicitWidth: 200
        Rectangle{
            id:menuItemBackgroundRect
        //implicitHeight: parent.items.size() * 60
            anchors.fill: parent
            color: "#f5f5f5"
            border.color: "#d5d5d5"
        }


        DropShadow{
            anchors.fill: menuItemBackgroundRect
            horizontalOffset: 3
            verticalOffset: 3
            radius: 8.0
            samples: 17
            color: "#80000000"
            source: menuItemBackgroundRect
        }
    }

    delegate: MenuItem{
        id:menuItem
        implicitHeight: 26

        indicator: Item{
            id: element7
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            implicitWidth: 40


            //Rectangle{
            //    anchors.fill: parent
            //    color: "green"
            //}

            Image {
                width: 18
                height: 18
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter

                smooth: true
                antialiasing: true
                fillMode: Image.PreserveAspectFit
                source: menuItem.action.icon.source
            }
            //Rectangle{
            //    width: 20
            //    height: 20
            //    color: "red"
            //}
        }

        contentItem: Item{
            anchors.left: parent.left
            anchors.leftMargin: menuItem.indicator.width
            anchors.right: parent.right
            anchors.rightMargin: 10

            Text {
                //anchors.rightMargin: menuItem.arrow.width
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter

                text: menuItem.text
                font.pointSize: 10
                opacity: enabled ? 1.0 : 0.3
                color: "#000000"
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }

            Text {
                //anchors.rightMargin: menuItem.arrow.width
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter

                text: (menuItem.action.shortcut === undefined ? "" : menuItem.action.shortcut)
                font.pointSize: 10
                opacity: enabled ? 1.0 : 0.3
                color: "#000000"
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }
        }

        background: Rectangle{
            anchors.fill: parent
            color: menuItem.highlighted ? "#91c9f7" : "transparent"
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
