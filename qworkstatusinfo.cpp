﻿#include "qworkstatusinfo.h"
#include "datareceiver.h"

WorkStatusInfoModel::WorkStatusInfoModel(QObject* parent) : QAbstractListModel(parent){
    for(int i = 0; i < rowCount(createIndex(0, 0)); ++i){
        m_valueChanged.append(false);
    }
}

void WorkStatusInfoModel::connectToReceiver(DataReceiver* receiver){
    connect(receiver, &DataReceiver::workStatus, this, &WorkStatusInfoModel::processWorkStatusInfo);
}

int WorkStatusInfoModel::rowCount(const QModelIndex&) const{
    return 12;
}

QVariant WorkStatusInfoModel::data(const QModelIndex& index, int role) const{
    if(role == TitleRole){
        return Titles[index.row()];
    }else if(role == ValueRole){
        switch (index.row()) {
        case 0:
            return m_wsi.freqEnough;
        case 1:
            return m_wsi.dataInRange;
        case 2:
            return m_wsi.dataStable;
        case 3:
            return m_wsi.detectBufferEnough;
        case 4:
            return m_wsi.bufPoolState;
        case 5:
            return m_wsi.bufferPercent;
        case 6:
            return m_wsi.algorithmState;
        case 7:
            return m_wsi.report;
        case 8:
            return m_wsi.dataFreq;
        case 9:
            return m_wsi.dataAmount;
        case 10:
            return m_wsi.port;
        case 11:
            return m_wsi.usbConnected;
        }
    }else if(role == IsSameRole){
        return m_valueChanged[index.row()];
    }
}

QHash<int, QByteArray> WorkStatusInfoModel::roleNames() const{
    QHash<int, QByteArray> roles;
    roles[TitleRole] = "title";
    roles[ValueRole] = "value";
    roles[IsSameRole] = "same";
    return roles;
}

void WorkStatusInfoModel::processWorkStatusInfo(const WorkStatusInfo& wsi){
    m_valueChanged[0] = wsi.freqEnough == m_wsi.freqEnough;
    m_valueChanged[1] = wsi.dataInRange == m_wsi.dataInRange;
    m_valueChanged[2] = wsi.dataStable == m_wsi.dataStable;
    m_valueChanged[3] = wsi.dataAmount == m_wsi.dataAmount;
    m_valueChanged[4] = wsi.bufPoolState == m_wsi.bufPoolState;
    m_valueChanged[5] = wsi.bufferPercent == m_wsi.bufferPercent;
    m_valueChanged[6] = wsi.algorithmState == m_wsi.algorithmState;
    m_valueChanged[7] = wsi.report == m_wsi.report;
    m_valueChanged[8] = wsi.dataFreq == m_wsi.dataFreq;
    m_valueChanged[9] = wsi.dataAmount == m_wsi.dataAmount;
    m_valueChanged[10] = wsi.port == m_wsi.port;
    m_valueChanged[11] = wsi.usbConnected == m_wsi.usbConnected;

    m_wsi = wsi;
\
    int max = rowCount(createIndex(0, 0));
    dataChanged(createIndex(0, 0), createIndex(max, max));
}
