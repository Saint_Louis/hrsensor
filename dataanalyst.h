﻿#ifndef DATAANALYST_H
#define DATAANALYST_H

#include <QObject>
#include <QList>

class NormalWeightedAverager;

class DataAnalyst : public QObject{
    Q_OBJECT
public:
    DataAnalyst(QObject* parent = nullptr);
    Q_INVOKABLE void input(float time, float value);
    Q_INVOKABLE void clear();

signals:
    void heartBeat(float time, float value);
    void meanHeartBeat(float time, float value);
    void normalHeartBeat(float time, float value);
    void offset(float time, float value);

private:
    NormalWeightedAverager* m_averager;
    float m_previousHb = 0;
    float m_lastTime = 0;
    bool m_hasValue = false;

    //窗口大小，默认30个数据（30秒）
    int m_wndSize = 20;
    //数据窗口，用于求平均数
    QList<float> m_wnd;

    float meanHeartBeat() const;
    void enquene(float f);
};

#endif // DATAANALYST_H
