﻿#include "baseplotitem.h"

BasePlotItem::BasePlotItem(QQuickItem* parent) : QQuickPaintedItem(parent), m_customPlot(nullptr) {
    setFlag(QQuickItem::ItemHasContents, true);
    setAcceptedMouseButtons(Qt::AllButtons);

    connect(this, &::QQuickPaintedItem::widthChanged, this, &BasePlotItem::updateCustomPlotSize);
    connect(this, &::QQuickPaintedItem::heightChanged, this, &BasePlotItem::updateCustomPlotSize);
}

BasePlotItem::~BasePlotItem(){
    if(m_customPlot != nullptr){
        delete m_customPlot;
        m_customPlot = nullptr;
    }
}

void BasePlotItem::paint(QPainter* painter){
    if(m_customPlot){
        QPixmap pic(boundingRect().size().toSize());
        QCPPainter qcppainter(&pic);

        m_customPlot->toPainter(&qcppainter);
        painter->drawPixmap(QPoint(), pic);
    }
}

void BasePlotItem::beginPlot(float time){
    m_time = time;
}

void BasePlotItem::endPlot(){
    m_customPlot->yAxis->rescale();
    m_customPlot->replot();
}

void BasePlotItem::initCustomPlot(){
    m_customPlot = new QCustomPlot();
    updateCustomPlotSize();
    m_customPlot->setBackground(QBrush(QColor(245,245,245)));

    //初始化样式
    m_customPlot->addGraph();
    m_customPlot->graph(0)->setPen(QPen(Qt::darkGray));
    m_customPlot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, QPen(Qt::darkGray, 1), QBrush(Qt::white), 5));
    m_customPlot->graph(0)->setName(QStringLiteral("测量心率"));

    m_customPlot->addGraph();
    m_customPlot->graph(1)->setPen(QPen(Qt::blue));
    m_customPlot->graph(1)->setName(QStringLiteral("偏移值"));

    m_customPlot->addGraph();
    m_customPlot->graph(2)->setPen(QPen(Qt::cyan));
    m_customPlot->graph(2)->setName(QStringLiteral("平均心率"));

    m_customPlot->addGraph();
    m_customPlot->graph(3)->setPen(QPen(Qt::magenta));
    m_customPlot->graph(3)->setName(QStringLiteral("正态权重心率"));

    m_customPlot->addGraph();
    m_customPlot->graph(4)->setPen(QPen(Qt::red, 1.5));
    m_customPlot->graph(4)->setName(QStringLiteral("用户查看心率"));

    m_customPlot->axisRect()->setupFullAxesBox();
    m_customPlot->xAxis->setLabel("X");
    m_customPlot->yAxis->setLabel("Y");

    m_customPlot->legend->setVisible(true);
    m_customPlot->legend->setFont(QFont("Helvetica", 9));
    m_customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignLeft | Qt::AlignTop);

    m_customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectLegend | QCP::iSelectPlottables);
    m_customPlot->axisRect()->setRangeDrag(Qt::Horizontal);
    m_customPlot->axisRect()->setRangeZoom(Qt::Horizontal);

    connect(m_customPlot, &QCustomPlot::afterReplot, this, &BasePlotItem::onCustomReplot);
    m_customPlot->replot();
}

void BasePlotItem::appendHeartBeat(double value){
    m_customPlot->graph(0)->addData(m_time, value);
}

void BasePlotItem::appendOffset(double value){
    m_customPlot->graph(1)->addData(m_time, value);
}

void BasePlotItem::appendMean(double value){
    m_customPlot->graph(2)->addData(m_time, value);
}

void BasePlotItem::appendNormal(double value){
    m_customPlot->graph(3)->addData(m_time, value);
}

void BasePlotItem::appendUserHreatBeat(double value)
{
    m_customPlot->graph(4)->addData(m_time, value);
}

void BasePlotItem::clear(){
    m_customPlot->graph(0)->data().data()->clear();
    m_customPlot->graph(1)->data().data()->clear();
    m_customPlot->graph(2)->data().data()->clear();
    m_customPlot->graph(3)->data().data()->clear();
    m_customPlot->graph(4)->data().data()->clear();
    m_customPlot->replot();
}

//double BasePlotItem::getMaxTime() const{
//    return m_maxTime;
//}
//
//double BasePlotItem::getTimeScale() const{
//    return m_timeScale;
//}
//
//void BasePlotItem::setTimeScale(double ts){
//    m_timeScale = ts;
//    m_customPlot->xAxis->setRange(getNavigation(), getNavigation() + getTimeScale());
//}
//
//double BasePlotItem::getNavigation() const{
//    return m_navigation;
//}
//
//void BasePlotItem::setNavigation(double nav){
//    m_navigation = nav;
//    m_customPlot->xAxis->setRange(getNavigation(), getNavigation() + getTimeScale());
//}

void BasePlotItem::routeMouseEvent(QMouseEvent* event){
    if(m_customPlot){
        QMouseEvent* newEvent = new QMouseEvent(event->type(), event->localPos(), event->button(), event->buttons(), event->modifiers());
        QCoreApplication::postEvent(m_customPlot, newEvent);
    }
}

void BasePlotItem::mousePressEvent(QMouseEvent* event){
    routeMouseEvent(event);
}

void BasePlotItem::mouseReleaseEvent(QMouseEvent* event){
    routeMouseEvent(event);
}

void BasePlotItem::mouseMoveEvent(QMouseEvent* event){
    routeMouseEvent(event);
}

void BasePlotItem::mouseDoubleClickEvent(QMouseEvent* event){
    routeMouseEvent(event);
}

void BasePlotItem::wheelEvent(QWheelEvent* event){
    QWheelEvent* _event = new QWheelEvent(event->pos(), event->delta(), event->buttons(), event->modifiers(), event->orientation());
    QCoreApplication::sendEvent(m_customPlot, _event);
}

void BasePlotItem::updateCustomPlotSize(){
    if(m_customPlot){
        m_customPlot->setGeometry(0, 0, width(), height());
    }
}

void BasePlotItem::onCustomReplot(){
    update();
}
