﻿//#include <QGuiApplication>
#include <QtWidgets/QApplication>
#include <QQmlApplicationEngine>


#include "datareceiver.h"
#include "datarecoder.h"
#include "baseplotitem.h"
#include "dataanalyst.h"
#include "qworkstatusinfo.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);
    app.setOrganizationName("vrbeyond");
    app.setOrganizationDomain("www.vrbeyond.com");

    QQmlApplicationEngine engine;

    //注册自定义类型
    qmlRegisterType<WorkStatusInfoModel>("Louis.HRSensor", 1, 0, "WorkStatusInfoModel");
    qmlRegisterType<DataReceiver>("Louis.HRSensor", 1, 0, "DataReceiver");
    qmlRegisterType<DataRecoder>("Louis.HRSensor", 1, 0, "DataRecoder");
    qmlRegisterType<BasePlotItem>("Louis.HRSensor", 1, 0, "CustomPlot");
    qmlRegisterType<DataAnalyst>("Louis.HRSensor", 1, 0, "DataAnalyst");

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
