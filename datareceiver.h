﻿#ifndef DATARECEIVER_H
#define DATARECEIVER_H
#include <QObject>
#include <QtNetwork>
#include <QVariantList>
#include "qworkstatusinfo.h"

//for QDateTime
#include <QDateTime>

class QWorkStatusInfo;

//工控状态-缓冲池状态
enum WorkStatusBufferPoolState{
    WSBPS_Unkonw,
    //等待预检通过中
    WSBPS_Waiting,
    //缓冲数据中
    WSBPS_Buffering,
    //满足最小可识别心率数量
    WSBPS_Min,
    //最大数据池量
    WSBPS_Full
};
class DataReceiver : public QObject{
    Q_OBJECT
public:
    DataReceiver(QObject* parent = nullptr);
    ~DataReceiver();

signals:
    void heartBeatDataReceive(const QDateTime& time, float value);
    void rawDataReceive(const QVariantList& arr);

    void workStatus(const WorkStatusInfo& wsi);

private:
    QUdpSocket* m_udpSock;

    void ParseJsonData(const std::string& str);

private slots:
    void ProcessReadyRead();
};

#endif // DATARECEIVER_H
