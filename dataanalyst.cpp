﻿#include "dataanalyst.h"
#include "normalweightedaverager.h"
#include <cmath>
#include <QDebug>

DataAnalyst::DataAnalyst(QObject* parent) : QObject(parent), m_averager(new NormalWeightedAverager(this)) {}

void DataAnalyst::input(float time, float value){
    emit heartBeat(time, value);
    if(m_hasValue){
        float gap = abs(value - m_previousHb);
        emit offset(time, gap);
    }else{
        emit offset(time, 0);
    }

    m_previousHb = value;
    m_hasValue = true;
    m_lastTime = time;

    //输入均值滤波器中，获取均值滤波后的心率
    enquene(value);
    emit meanHeartBeat(time, meanHeartBeat());

    //输入正态滤波器中，计算正态权重均值滤波后的心率
    m_averager->input(static_cast<double>(value));
    emit normalHeartBeat(time, static_cast<float>(m_averager->calculate()));
}

void DataAnalyst::clear(){
    m_previousHb = 0;
    m_hasValue = false;
    m_wnd.clear();
}

float DataAnalyst::meanHeartBeat() const{
    float sum = 0;
    for(float f : m_wnd) sum += f;
    return sum / m_wnd.size();
}

void DataAnalyst::enquene(float f){
    m_wnd.push_back(f);
    if(m_wnd.size() > m_wndSize){
        m_wnd.pop_front();
    }
}
