﻿#ifndef QWORKSTATUSINFO_H
#define QWORKSTATUSINFO_H

#include <QObject>
#include <QDateTime>
#include <QAbstractListModel>

class DataReceiver;

//工控状态数据结构
struct WorkStatusInfo{
    //时间戳
    QDateTime timeStamp;
    //USB设备是否连接
    bool usbConnected;
    //数据包频率是否足够
    bool freqEnough;
    //数据范围是否有效
    bool dataInRange;
    //数据是否稳定
    bool dataStable;
    //预测数据是否充足
    bool detectBufferEnough;
    //缓冲池状态
    QString bufPoolState;
    //缓冲进度
    int bufferPercent;
    //算法工作状态
    QString algorithmState;
    //文本化状态报告（暂留）
    QString report;
    //目前数据接收频率
    int dataFreq;
    //目前数据量
    int dataAmount;
    //USB设备所在COM口
    QString port;

    WorkStatusInfo() : timeStamp(QDateTime()), usbConnected(false), freqEnough(false),
        dataInRange(false), dataStable(false), detectBufferEnough(false), bufPoolState(),
        bufferPercent(0), algorithmState(), dataFreq(0), dataAmount(0), port() {}
};

class WorkStatusInfoModel : public QAbstractListModel{
    Q_OBJECT
public:
    enum WorkStatusInfoRoles{
        TitleRole = Qt::UserRole + 1,
        ValueRole, IsSameRole
    };

public:
    WorkStatusInfoModel(QObject* parent = nullptr);
    Q_INVOKABLE void connectToReceiver(DataReceiver* receiver);

    int rowCount(const QModelIndex&) const override;
    QVariant data(const QModelIndex& index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

public slots:
    void processWorkStatusInfo(const WorkStatusInfo& wsi);

private:
    inline const static QList<QString> Titles{
        "freq", "range", "stability", "amount", "pool", "percent", "algorithm", "report", "data_freq", "data_amount", "port", "is_connected"
    };

    WorkStatusInfo m_wsi;
    QList<bool> m_valueChanged;
};

#endif // QWORKSTATUSINFO_H
