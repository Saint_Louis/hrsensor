﻿#include "datarecoder.h"
#include <QFile>
#include <QTextStream>
#include <QTextCodec>
#include <QDebug>

DataRecoder::DataRecoder(QObject *parent) : QObject(parent){}

void DataRecoder::beginLog(float time){
    if(!m_loging){
        m_loging = true;
        m_dataPoints.push_back({time, -1, -1, -1, -1});
    }else{
        qDebug() << "DataRecoder::beginLog(): can't beginLog without endLog() called, did you forgot endLog()?";
    }
}

void DataRecoder::endLog(){
    m_loging = false;
}

void DataRecoder::appendHeartBeat(float value){
    //m_hbRecord.push_back(std::make_pair(time, value));
    m_dataPoints.back().rawHb = value;
}

//void DataRecoder::appendOffset(float value){
//    m_offsetRecord.push_back(std::make_pair(time, value));
//}

void DataRecoder::appendMean(float value){
    //m_meanHeatBeat.push_back(std::make_pair(time, value));
    m_dataPoints.back().meanHb = value;
}

void DataRecoder::apeendNormalHeartBeat(float value){
    //m_normalHeatBeat.push_back(std::make_pair(time, value));
    m_dataPoints.back().normalHb = value;
}

void DataRecoder::appendUserHeartBeat(float value){
    m_dataPoints.back().userHb = value;
}

void DataRecoder::clear(){
    //m_hbRecord.clear();
    //m_offsetRecord.clear();
    //m_meanHeatBeat.clear();
    //m_normalHeatBeat.clear();
    m_dataPoints.clear();
}

int DataRecoder::size() const{
    //return m_hbRecord.size();
    return m_dataPoints.size();
}

bool DataRecoder::saveToFile(const QString& path) const{
    QFile file(path);
    if(file.open(QIODevice::WriteOnly | QIODevice::Text)){
        QTextStream textStream(&file);
        textStream.setCodec(QTextCodec::codecForName("GBK"));
        textStream << QStringLiteral("时间,心率,平均心率,正态均值心率,用户查看心率\n");

        for(const DataPoint& dp : m_dataPoints){
            textStream << dp.time << "," << dp.rawHb << "," << dp.meanHb << "," << dp.normalHb << ",";
            if(dp.userHb != -1) textStream << dp.userHb;
            textStream << "\n";
        }
        //for(int i = 0; i < std::min(m_hbRecord.size(), m_offsetRecord.size()); ++i){
        //    if(m_hbRecord[i].first != m_offsetRecord[i].first){
        //        qDebug() << "记录信息未对齐";
        //    }
        //    textStream << m_hbRecord[i].first << "," << m_hbRecord[i].second << ","
        //               << m_offsetRecord[i].second << "," << m_meanHeatBeat[i].second << ",\n";
        //}
        file.close();
        return true;
    }else{
        return false;
    }
}
