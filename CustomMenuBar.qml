﻿import QtQuick 2.0
import QtQuick.Controls 2.5

MenuBar {
    background: Rectangle{
        implicitHeight: 20
        implicitWidth: 40
        color: "#ffffff"
    }

    delegate: MenuBarItem{
        id: menuBarItem
        font.bold: true
        font.pointSize: 10

        function replaceText(txt) {
            var index = txt.indexOf("&");
            if(index >= 0)
                txt = txt.replace(txt.substr(index, 2), ("<u>" + txt.substr(index + 1, 1) +"</u>"));
            return txt;
        }

        contentItem: Text{
            text: replaceText(menuBarItem.text)
            font.pointSize: 10
            opacity: enabled ? 1.0 : 0.3
            color: "#000000"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        background: Rectangle{
            implicitHeight: 20
            implicitWidth: 40
            color: menuBarItem.highlighted ? "#91c9f7" : "#ffffff"
        }
    }
}
