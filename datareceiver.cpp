﻿#include "datareceiver.h"
#include <json.hpp>
#include <QDebug>

DataReceiver::DataReceiver(QObject* parent) : QObject(parent){
    m_udpSock = new QUdpSocket(this);
    m_udpSock->bind(QHostAddress::LocalHost, 12345);
    connect(m_udpSock, SIGNAL(readyRead()), this, SLOT(ProcessReadyRead()));
    qDebug() << QStringLiteral("套接字初始化完毕");
}

DataReceiver::~DataReceiver(){
    qDebug() << QStringLiteral("DataReceiver::~DataReceiver()");
}

void DataReceiver::ParseJsonData(const std::string& str){
    //qDebug() << QString(str.c_str());
    nlohmann::json json;

    try {
        json = nlohmann::json::parse(str);
    } catch(...) {
        return;
    }

    //识别工控状态信息
    if(json["data_type"].get<std::string>() == "status"){
        WorkStatusInfo wsi;

        wsi.timeStamp = QDateTime::fromString(QLatin1String(json["time"].get<std::string>().c_str()));
        wsi.usbConnected = json["is_connected"].get<bool>();
        if(wsi.usbConnected){
            wsi.freqEnough = json["freq"].get<bool>();
            wsi.dataInRange = json["range"].get<bool>();
            wsi.dataStable = json["stability"].get<bool>();
            wsi.detectBufferEnough = json["amount"].get<bool>();
            wsi.bufferPercent = json["percent"].get<int>();
            wsi.algorithmState = QString::fromStdString(json["algorithm"].get<std::string>());
            wsi.bufPoolState = QString::fromStdString(json["pool"].get<std::string>());
            wsi.dataFreq = json["data_freq"].get<int>();
            wsi.dataAmount = json["data_amount"].get<int>();
            wsi.port = QString::fromStdString(json["port"].get<std::string>());
        }
        emit workStatus(wsi);
    }
    //识别采集信息
    else if(!json["sensor_type"].is_null()){
        auto sensorType = json["sensor_type"].get<std::string>();
        auto dateType = json["data_type"].get<std::string>();

        //获取计算心率值
        if(dateType == "cal" && sensorType == "hb"){
            QString strTime = QString(json["time"].get<std::string>().c_str());
            strTime = strTime.mid(0, strTime.lastIndexOf("."));
            QDateTime time = QDateTime::fromString(strTime, QStringLiteral("yyyy-MM-dd hh:mm:ss"));
            emit heartBeatDataReceive(time, json["bpm"].get<float>());
        }
    }
    else if(!json["series"].is_null()){
        auto& series = json["series"];
        QVariantList list;

        for(auto& el : series){
            list.push_back(el.get<int>());
        }

        emit rawDataReceive(list);
    }
}

void DataReceiver::ProcessReadyRead(){
    QByteArray datagram;
    while(m_udpSock->hasPendingDatagrams()){
        int size = static_cast<int>(m_udpSock->pendingDatagramSize());
        datagram.resize(size);
        m_udpSock->readDatagram(datagram.data(), datagram.size());
        ParseJsonData(datagram.toStdString());
    }
    //auto json = nlohmann::json::parse(str);
}
