# HRSensor
## 项目概述
该项目将生理传感器数据进行可视化和图表表示。是对生理传感器项目的技术支持和数值测试的实验性质项目。  
HRSensor会记录、计算生理数据，并且绘制成图表。目前支持的数据有：

- 测量心率
- 测量心率差
- 30秒均值心率（滤波1）
- 30秒标准正态加权平均心率（滤波2）
- 10秒平均心率（基于滤波2）

上述支持的数据除了测量心率差，均会被程序记录，支持保存为csv格式。

## 编译环境
QT 5.12.6，需要Qt Charts

## 工控信息
工控信息使用json协议传输，以1HZ的评论传输。  
工控信息表Ver 0.2：

| json字段 | 字段类型 | 数据范围 | 说明 |
| - | - | - | - |
| freq | bool | true/false | 接收数据的频率是否符合最低频率标准 |
| range | bool | true/false | 探测器数据是否符合范围 |
| stablity | bool | true/false | 数据是否稳定（通过均方差） |
| amount | bool | true/false | 数据量是否符合要求 |
| pool | string | waiting、buffering、min、full | 数据池缓冲状态 |
| percent | int | 0~100 | 数据池缓冲进度 |
| algorithm | string | waiting、working | 测量心率算法工作状态 |
| report | string |  | 文本化报告 |
| data_freq | int |  | 目前数据接收频率 |
| data_amount | int |  | 目前数据量 |
| port | string | COM[1~N] | USB设备所在COM口 |
| is_connected | bool | true/false | USB设备是否连接 |

### 关于工控信息的数据格式
基于之测量数据的传输格式：
> {"data_type": "status", "sensor_type": "self_check", "time": "2020-04-24 10:38:07.379", "freq": true, "range": false, "stability": false, "amount": true, "pool": "waiting", "percent": 0, "algorithm": "waiting", "report": "", "data_freq": 35, "data_range": 205330, "data_stability": 30650, "data_amount": 167, "port": "COM3", "is_connected": true}
